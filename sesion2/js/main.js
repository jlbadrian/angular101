angular.module('formRegistro', [])
.controller('FormRegistroCtrl', ['$scope', 'userapi', function($scope, userapi) {
	var currentid = 0;
	$scope.userinfo = {};
	$scope.userlist = userapi.list;
	$scope.nuevoRegistro = function() {
		$scope.userinfo = {};
		currentid = 0;
	}
	$scope.guardarRegistro = function() {
		if (currentid) {
			userapi.edit(currentid, $scope.userinfo);
		} else {
			currentid = userapi.add($scope.userinfo);
		}
		console.log($scope.userinfo);
	}
	$scope.editarRegistro = function(id) {
		var reg = userapi.get(id);
		if (reg)
			$scope.userinfo = reg;
	}
	$scope.eliminarRegistro = function(id) {
		userapi.remove(id);
		if (currentid == id)
			$scope.nuevoRegistro();
	}
}])
