angular.module('formRegistro')
.factory('userapi', ['$q', function($q) {
	this.index = 0;
	this.list = [];
	if (typeof(Storage) !== "undefined") {
		if (localStorage.getItem('users.list') === null){
			localStorage.setItem('users.list', angular.toJson(this.list));
		}     
		if (localStorage.getItem('users.index') === null){
			localStorage.setItem('users.index', "0");
		}
		this.list = angular.fromJson(localStorage.getItem('users.list'));
		this.index = Number(localStorage.getItem('users.index'));
	}
	this.updateStorage = function() {
		if (typeof(Storage) !== "undefined") {
			localStorage.setItem('users.list', angular.toJson(this.list));
			localStorage.setItem('users.index', this.index);
		}
	}
	this.add = function(obj) {
		obj.id = ++this.index;
		this.list.push(obj);
		this.updateStorage();
		return this.index;
	}
	this.get = function(id) {
		for (var i = 0; i < this.list.length; i++) {
			if (this.list[i].id == id) {
				return this.list[i];
				break;
			}
		}
		return false;
	}
	this.remove = function(id) {
		for (var i = 0; i < this.list.length; i++) {
			if (this.list[i].id == id) {
				this.list.splice(i, 1);
				break;
			}
		}
		this.updateStorage();
	}
	this.edit = function(id, obj) {
		for (var i = 0; i < this.list.length; i++) {
			if (this.list[i].id == id) {
				angular.extend(this.list[i], obj);
				this.list[i].id = id;
				break;
			}
		}
		this.updateStorage();
	}
	return this;
}])