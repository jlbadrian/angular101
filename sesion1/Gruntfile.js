module.exports = function (grunt) {

  grunt.loadNpmTasks('grunt-contrib-watch');


// Project configuration.
grunt.initConfig({
  pkg: grunt.file.readJSON('package.json'),
  builddir: '.',
  buildtheme: '',
  watch: {
    files: [
    '**/*.js',
    '**/*.htm',
    '**/*.html',
    '**/*.php',
    '**/*.css',
    '**/*.ini',
    '!components/**',
    '!node_modules/**',
    '!bower_components/**',
    '!components/**',
    '!test/**',
    '!tests/**'],
    tasks: 'none',
    options: {
      livereload: true,
      nospawn: true
    }
  }
});

grunt.registerTask('none', function() {});

grunt.registerTask('default', ['watch']);
};