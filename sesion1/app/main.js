/**
* Angular101 Module
*
* Description
*/
angular.module('Angular101', ['ngRoute'])
.config(['$routeProvider',function($routeProvider) {
	$routeProvider
	.when('/',{
		controller:"MyCtrl",
		templateUrl:"views/myview.html"
	})
	.when('/test',{
		controller:"MyCtrl",
		templateUrl:"views/testview.html"
	});
}])
.controller('MyCtrl', ['$scope', function($scope){
	$scope.title='Introduccion';
}])
.run(function(){
	console.log('empezo angular');
})